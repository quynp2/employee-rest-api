package com.devcamp.employeerestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;

    public Employee() {
    }

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String name() {
        return firstName + lastName;
    }

    public int getSalary() {
        return salary;
    }

    public int getAnnualSalary() {
        return salary * 12;
    }

    public int raiseSalary(int percent) {
        return salary = this.salary * (100 + percent) / 100;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", salary=" + salary
                + "]";
    }

    @GetMapping("/employees")
    public ArrayList<Employee> Employeelist() {
        ArrayList<Employee> newEmployee = new ArrayList<Employee>();

        Employee nv01 = new Employee(001, "Hoang", "Nghia", 10000);
        Employee nv02 = new Employee(002, "Quy", "Nguyen", 200000);
        Employee nv03 = new Employee(003, "Daisy", "Nguyen", 3000000);

        newEmployee.add(nv01);
        newEmployee.add(nv02);
        newEmployee.add(nv03);

        return newEmployee;

    }

}
